Consists of the data microservice repos that were used for the **initial** iteration of the Luckyspells project.

To clone the project, run the following:

```
git clone git@gitlab.com:oawofolu/luckyspells.git
cd luckyspells
git submodule init
git submodule update
```

<img src="screenshots/version0.jpg" alt="Architecture Overview"/>